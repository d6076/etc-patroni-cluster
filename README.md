# etcd-patroni-cluster

Запуск кластера patroni с key-value хранилищем etcd и реверс прокси HAproxy

## Getting started

Для развертывания используйте плэйбук main.yaml
```
ansible-playbook main.yaml -b
```

Чтобы перезапустить кластер etcd используйте плэйбук etcd-restart.yaml
```
ansible-playbook etcd-restart.yaml -b
```
![Cluster logo](https://gitlab.com/d6076/etc-patroni-cluster/-/raw/main/patroni-ectd-cluster.jpg?raw=true)
